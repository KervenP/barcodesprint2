/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL.IDAO;

import Util.GenerateJsonBarcode;
import com.google.gson.JsonElement;
import java.util.List;

/**
 *
 * @author PolidorArte
 */
public interface IEncodingHistory {
        public void mongoConnection(String connectionString);
        public void insertBarcodeDecimalAndBinary(JsonElement saveJson);
         public List<GenerateJsonBarcode> fetchAllFromMongoDb();
}
