/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL.IDAO;

import java.util.List;
import java.util.Set;

/**
 *
 * @author PolidorArte
 */
public interface ISQL_DAO {

    public void populate();

    public List<Integer> fetchAllCodesByCountryName(String countryName);

    public Set<String> fetchAllCountries();

}
