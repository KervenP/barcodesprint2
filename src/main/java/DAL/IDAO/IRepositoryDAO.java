/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL.IDAO;


import DAL.EAN13_EncodingPattern;
import java.util.Optional;

/**
 *
 * @author PolidorArte
 */
public interface IRepositoryDAO {

    //Return The pattern to decode the decimal code into binary code
  public EAN13_EncodingPattern fetchEAN13EncodingPattern(byte digit);

    //return the encodingPattern for each psart of the decimalCode
    public String fetchLCode(byte digit);

    public String fetchGCode(byte digit);

    public String fetchRCode(byte digit);

   // public List<Barcode> ReturnAListOfBarcodeFromMockNeat();

}
