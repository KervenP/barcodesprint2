/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

/**
 *
 * @author PolidorArte
 */
public class EAN13_EncodingPattern {

    private byte countryDigit;
    private String leftGroupPattern;
    private String rightGroupPattern;

    public EAN13_EncodingPattern(byte countryDigit, String leftGroupPattern, String rightGroupPattern) {
        this.countryDigit = countryDigit;
        this.leftGroupPattern = leftGroupPattern;
        this.rightGroupPattern = rightGroupPattern;
    }

    public EAN13_EncodingPattern() {
    }

    public int getCountryDigit() {
        return countryDigit;
    }

    public void setCountryDigit(byte countryDigit) {
        this.countryDigit = countryDigit;
    }

    public String getLeftGroupPattern() {
        return leftGroupPattern;
    }

    public void setLeftGroupPattern(String leftGroupPattern) {
        this.leftGroupPattern = leftGroupPattern;
    }

    public String getRightGroupPattern() {
        return rightGroupPattern;
    }

    public void setRightGroupPattern(String rightGroupPattern) {
        this.rightGroupPattern = rightGroupPattern;
    }

}
