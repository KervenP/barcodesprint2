/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DAL.IDAO.IEncodingHistory;
import Util.GenerateJsonBarcode;
import com.google.gson.JsonElement;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

/**
 *
 * @author PolidorArte mangodbConnection step 1-declare mongoClient 2-declare
 * mongoDatabas 3-delcare connectionString 4-declare mongoClientUri 5-insert the
 * connectionstring in the client Uri 6-insert the uri in the new instance of
 * mongoclient 7.with the mongoClient Object getDatabase(databaseName) 8-
 */
public class History_JSON_DAO implements IEncodingHistory {

    MongoClient mongoClient;
    MongoDatabase db;
    String user = "colvalse";
    String password = "NewLife-001";
    String databaseName = "colvalse_encodinghistory";
    String host = "mongodb-colvalse.alwaysdata.net:27017";
    String connectionString = "mongodb://" + user + ":"
            + password + "@" + host + "/" + databaseName;
    // connectionString = connectionString + "?retryWrites=true";

    public History_JSON_DAO() {
        mongoConnection(connectionString);
    }

    @Override
    public void mongoConnection(String connectionString) {
        MongoClientURI uri = new MongoClientURI(connectionString);
        mongoClient = new MongoClient(uri);
        db = mongoClient.getDatabase(databaseName);
        // DBCollection collection= db.getCollection("encodingHistory");

    }
    // Cette base contient la collection EncodingHistory dont chaque document enregistre une paire le barcode décimal et sa traduction binaire (decimalBarcode, binaryBarcode). 

    @Override
    public void insertBarcodeDecimalAndBinary(JsonElement saveJson) {
        MongoCollection coll = db.getCollection("encodingHistory");
        Document newdoc = Document.parse(saveJson.toString());
        coll.insertOne(newdoc);
    }

    @Override
    public List<GenerateJsonBarcode> fetchAllFromMongoDb() {
        List<GenerateJsonBarcode> alldocs = new ArrayList();
       
        MongoIterable<String> allCollections = db.listCollectionNames();
        for (String collectionName : allCollections) {
            MongoCollection<Document> collection;
            collection = this.db.getCollection(collectionName);
            FindIterable<Document> findIterable = collection.find();
            for (Document d : findIterable) {
                GenerateJsonBarcode jb = new GenerateJsonBarcode();
                jb.setCountryName(d.getString("countryName"));
                jb.setDec(d.getString("dec"));
                jb.setBinary(d.getString("binary"));
                alldocs.add(jb);
            }
        }
        return alldocs;

    }
   
}
