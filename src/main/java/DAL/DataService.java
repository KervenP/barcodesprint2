/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DAL.IDAO.IEncodingHistory;
import DAL.IDAO.ISQL_DAO;
import DAL.IDAO.IRepositoryDAO;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author PolidorArte
 */
public class DataService {

    private IRepositoryDAO InMemoryDao;
    private static DataService Instance = null;
    private ISQL_DAO SQL_DAO;
    private IEncodingHistory EncodingHistory;

    private DataService() {

    }

    public static DataService getInstance() {
        if (Instance == null) {
            Instance = new DataService();
        }
        return Instance;
    }

    public ISQL_DAO getCountry_Sql_DAO() {
        return SQL_DAO = new SQL_DAO();
    }

    public IEncodingHistory getEncodingHistory() {
        return EncodingHistory = new History_JSON_DAO();
    }

    public IRepositoryDAO getInMemoryDAO() {
        return InMemoryDao = new InMemoryRepository();
    }
    
}
   
