/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DAL.IDAO.ISQL_DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.andreinc.mockneat.MockNeat;

/**
 *
 * @author PolidorArte
 */
public class SQL_DAO implements ISQL_DAO {

    String user = "colvalse", password = "NewLife-001", serverName = "mysql-colvalse.alwaysdata.net";
    int port = 3306;
    String db = "colvalse_barcode";

    public SQL_DAO(){}
    public Connection Connect() {
        String connectionString = "jdbc:mysql://" + serverName + ":3306/" + db + "?user=" + user + "&password=" + password;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(connectionString);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SQL_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conn;
    }

    public List<String> generateAmockListOfCountry() {
        MockNeat mock = MockNeat.threadLocal();
        List<String> countryName = mock.countries().names().list(20).val();
        return countryName;
    }

    public List<Integer> generateAmockListOfCountryCode() {
        MockNeat mock = MockNeat.threadLocal();
        List<Integer> countryCode = mock.intSeq().start(0).increment(1).list(10).val();
        return countryCode;
    }

    @Override
    public void populate() {
        String query;
        Statement st;
        try {

            st = Connect().createStatement();

            for (String s : generateAmockListOfCountry()) {
                for (Integer i : generateAmockListOfCountryCode()) {
                    query = "INSERT INTO barcode(country,EAN13Code) values ( " + s + "," + i + ")";
                    st.executeUpdate(query);

                }
            }
        } catch (SQLException ex) {
        }

    }

    @Override
    public Set<String> fetchAllCountries() {

        String query;
        Set<String> countryNames;
        Statement st;
        countryNames = new TreeSet();
        try {
            query = "SELECT * FROM barcode";
            st = Connect().createStatement();
            ResultSet rs = st.executeQuery(query);
            while (rs.next()) {
                String country = rs.getString("country");
                countryNames.add(country);
            }
            return countryNames;
        } catch (SQLException ex) {
            Logger.getLogger(SQL_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return countryNames;
    }

    @Override
    public List<Integer> fetchAllCodesByCountryName(String countryName) {
        String query;
     List<Integer>code=new ArrayList();
        Statement st;
        try{
            query="Select * FROM barcode WHERE country = "+ countryName;
        st=Connect().createStatement();
        ResultSet rs=st.executeQuery(query);
        while(rs.next())
        { int c=rs.getInt("EAN13Code");
              code.add(c);
            
        }
       return code;
     
        } catch (SQLException ex) {
            Logger.getLogger(SQL_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }

    ;

    /**
     *
     */
    @Override
    public void finalize() {
       
            try {
                super.finalize();
               Connect().close();
            } catch (Throwable ex) {
                Logger.getLogger(SQL_DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }    
}
