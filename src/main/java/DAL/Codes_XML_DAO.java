/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;


import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;


import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author PolidorArte
 */
public class Codes_XML_DAO {

    public static NodeList patternlist(String docEntry, String query) {
 
        try {
            //Build Dom
          //  File xmlEncodingPattern = new File();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true);
            DocumentBuilder dbBuilder = dbFactory.newDocumentBuilder();
            Document doc = dbBuilder.parse(docEntry);
           
           
            //Create Xpath
            
            XPathFactory xpathfactory = XPathFactory.newInstance();
            XPath xpath = xpathfactory.newXPath();
            //get the query info 
            XPathExpression expr = xpath.compile(query);

            //RunXpath query
            Object result = expr.evaluate(doc, XPathConstants.NODESET);
            NodeList nodes= (NodeList) result;
            return nodes;
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            //Logger.getLogger(XPathRunner.class.getName()).lod(Level.SEVERE,null,ex);

        }
  return null;
    }
  

}
