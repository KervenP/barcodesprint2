/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

/**
 *
 * @author PolidorArte
 */
public class EAN13_DigitEncoding {

    private byte digit;
    private String l_Code;
    private String g_Code;
    private String r_Code;

    public EAN13_DigitEncoding(byte digit, String l_Code, String g_Code, String r_Code) {
        this.digit = digit;
        this.l_Code = l_Code;
        this.g_Code = g_Code;
        this.r_Code = r_Code;
    }

    public int getDigit() {
        return digit;
    }

    public void setDigit(byte digit) {
        this.digit = digit;
    }

    public String getL_Code() {
        return l_Code;
    }

    public void setL_Code(String l_Code) {
        this.l_Code = l_Code;
    }

    public String getG_Code() {
        return g_Code;
    }

    public void setG_Code(String g_Code) {
        this.g_Code = g_Code;
    }

    public String getR_Code() {
        return r_Code;
    }

    public void setR_Code(String r_Code) {
        this.r_Code = r_Code;
    }

}
