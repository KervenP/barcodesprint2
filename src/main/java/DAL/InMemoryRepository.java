/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DAL.IDAO.IRepositoryDAO;
import org.w3c.dom.NodeList;

/**
 *
 * @author PolidorArte
 */
public class InMemoryRepository implements IRepositoryDAO {

    String pathEncodingPattern = "C:\\Users\\PolidorArte\\Documents\\NetBeansProjects\\BarcodeSprint2\\src\\main\\java\\DAL\\XMLFILE\\";
    String pathEncodingDigit="C:\\Users\\PolidorArte\\Documents\\NetBeansProjects\\BarcodeSprint2\\src\\main\\java\\DAL\\XMLFILE\\";
    public InMemoryRepository() {
        
    }

 @Override
    public EAN13_EncodingPattern fetchEAN13EncodingPattern(byte countryDigit) {
        EAN13_EncodingPattern ean13Pattern = new EAN13_EncodingPattern(countryDigit,
                getLPattern(countryDigit), getRPattern(countryDigit));
        return ean13Pattern;
    }
    
    public String getRPattern(byte digit) {
        String document = pathEncodingPattern + "XMLEndcodingPattern.xml";
        String query;
        query = "//EncodingPattern[@CountryDigit=\"" + digit + "\"]/@RightGroupPattern";
        NodeList result = Codes_XML_DAO.patternlist(document, query);
        return result.item(0).getNodeValue();
    }

    public String getLPattern(byte digit) {

        String document = pathEncodingPattern + "XMLEndcodingPattern.xml";
        String query;
        query = "//EncodingPattern[@CountryDigit=\"" + digit + "\"]/@LeftGroupPattern";
        NodeList result = Codes_XML_DAO.patternlist(document,query);
        return result.item(0).getNodeValue();
        }

    //Extra Functions 
    @Override
    public String fetchRCode(byte digit) {
        String document = pathEncodingDigit + "XMLEncodingDigit.xml";
        String query;
        //DigitEncoding[@Digit='1']//@R_Code
        query = "//DigitEncoding[@Digit=\"" + digit + "\"]/@R_Code";
        NodeList result = Codes_XML_DAO.patternlist(document,query);
        return result.item(0).getNodeValue();
    }

@Override
    public String fetchLCode(byte digit) {
         String document = pathEncodingDigit + "XMLEncodingDigit.xml";
        String query;
        query = "//DigitEncoding[@Digit=\"" + digit + "\"]/@L_Code";
        NodeList result = Codes_XML_DAO.patternlist(document, query);
        return result.item(0).getNodeValue();
    }

    @Override
    public String fetchGCode(byte digit) {
         String document = pathEncodingDigit + "XMLEncodingDigit.xml";
        String query;
        query = "//DigitEncoding[@Digit=\"" + digit + "\"]/@G_Code";
        NodeList result = Codes_XML_DAO.patternlist(document, query);
        return result.item(0).getNodeValue();
    }

}
