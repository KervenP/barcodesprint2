/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.Service;

import BLL.MDL.Barcode;
import BLL.MDL.EAN13_4Product;
import DAL.DataService;
import Util.EAN13_4ProductStub;
import Util.GenerateJsonBarcode;
import com.google.gson.JsonElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author PolidorArte
 */
public class BarcodeDecodingService {

    public static EAN13_4Product Prod;
    public static Barcode b;

    public static String getEAN13BinaryCodeForProduct(String countryId, String manufactureId, String productId) {

        return EAN13_4ProductStub.generateBinaryCode(countryId, manufactureId, productId);
    }

    /*   public static void PopulateDB(){
        DataService.getInstance().getCountry_Sql_DAO().populate();
    }*/
    public static List<String> fecthCodesByCountryName(String countryName) {
        List<String> convertCodetoString = new ArrayList();
        for (Integer i : DataService.getInstance().getCountry_Sql_DAO().fetchAllCodesByCountryName(countryName)) {
            convertCodetoString.add(String.valueOf(i));
        }
        return convertCodetoString;
    }

    public static Set<String> getAllCountryFromDB() {
        Set<String> countryNames = new TreeSet();
        countryNames = DataService.getInstance().getCountry_Sql_DAO().fetchAllCountries();
        return countryNames;
    }

    public static void insertDocToHistoryDao(JsonElement saveJson) {
        DataService.getInstance().getEncodingHistory().insertBarcodeDecimalAndBinary(saveJson);
    }

    public static List<GenerateJsonBarcode> listHistory() {
        return DataService.getInstance().getEncodingHistory().fetchAllFromMongoDb();
    }

}
