/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.MDL;

/**
 *
 * @author PolidorArte
 */
public class Barcode {

    private String country;
    private String manufacturer;
    private String product;
    private String decimalEncoding;
    private ByteBinaryData BinaryData;

    public Barcode(String country, String manufacturer, String product) {
        this.country = country;
        this.manufacturer = manufacturer;
        this.product = product;
        this.decimalEncoding = country+manufacturer+product;
        this.BinaryData = null;
    }

    public Barcode() {
      
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDecimalEncoding() {
        return decimalEncoding;
    }

    public void setDecimalEncoding(String decimalEncoding) {
        this.decimalEncoding = decimalEncoding;
    }

    public ByteBinaryData getBinaryData() {
        return BinaryData;
    }

    public void setBinaryData(ByteBinaryData BinaryData) {
        this.BinaryData = BinaryData;
    }

    @Override
    public String toString() {
        return "DecimalNumber Of The present Barcode \n[" + " country=" + country  + manufacturer + product+" ]";
    }

}
