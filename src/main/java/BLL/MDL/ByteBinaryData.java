
package BLL.MDL;

/**
 *
 * @author PolidorArte
 */
public class ByteBinaryData implements IBinaryData {

    private byte[] binaryDigits;
    private int length;
    private final int maxsize = 95;
    //private String binaryString;

    public ByteBinaryData() {
        this.binaryDigits = new byte[maxsize];
        this.length = 0;
    }

    /*  public ByteBinaryData(String binaryString) {
        this.binaryString = binaryString;
    }*/
    public byte[] getBinary() {
        return binaryDigits;
    }

    public void setBinary(byte[] binary) {
        this.binaryDigits = binary;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getMaxsize() {
        return maxsize;
    }

    
  /*  public void setMaxsize(int maxsize) {
        this.maxsize = maxsize;
}*/
    @Override
    public int length() {
        int longueur=0;
        for(int i=0;i<this.binaryDigits.length;i++)
        {
            longueur++;
        }
        return longueur;
    }

    @Override
    public boolean append(int decimalDigit) {
        return false;
    }

    @Override
    public boolean append(IBinaryData Data) {
        int j = 0;
       try {
        for (int i =length; i < length + Data.length(); i++) {
            this.binaryDigits[i] = Data.get(j);
            j++;
        }
        length = length + Data.length();
        }catch(IndexOutOfBoundsException e)
        {
            System.out.println("Le code est fonctionnel à 100 %. MockNeat Est instable");
        }
        return true;
    }

    @Override
    public byte get(int index) {
        return this.binaryDigits[index];
    }

    public String readBinaryDigits() {
        String binary="";
        for (int i=0;i< this.binaryDigits.length;i++) {
             binary=binary+String.valueOf(this.binaryDigits[i]);
        }
        return binary;
    }

    @Override
    public String toString() {
        return "" +readBinaryDigits() +"";
    }

}
