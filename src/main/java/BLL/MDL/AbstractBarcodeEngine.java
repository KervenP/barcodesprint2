/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.MDL;

import DAL.DataService;



/**
 *
 * @author PolidorArte
 */
public abstract class AbstractBarcodeEngine implements IBarcodeEngine {

    Barcode barcode;
    DataService Ds = DataService.getInstance();
    public AbstractBarcodeEngine(Barcode barcode) {
        this.barcode = barcode;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }
    
    
}
