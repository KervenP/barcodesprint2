/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.MDL;

/**
 *
 * @author PolidorArte
 */
public interface IBinaryData {
   public  int length();
   public boolean append(int decimalDigit);
   public boolean append(IBinaryData Data);
   public byte get(int index);   
    
}
