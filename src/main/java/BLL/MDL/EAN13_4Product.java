/**
 *
 * @author PolidorArte
 */
package BLL.MDL;

public class EAN13_4Product extends AbstractEAN13_Engine {

    public EAN13_4Product(Barcode barcode) {
        super(barcode);
    }

    @Override
    public Barcode getBarcode() {
        return barcode;
    }

    @Override
    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }

    @Override
    public IBinaryData encodeManufacturer() {
        ByteBinaryData data = new ByteBinaryData();
        byte[] binaryManufacturer = new byte[ShouldReturnAByteStringManufacturer().length()];

        for (int i = 0; i < ShouldReturnAByteStringManufacturer().length(); i++) {
            binaryManufacturer[i] = Byte.valueOf(String.valueOf(ShouldReturnAByteStringManufacturer().charAt(i)));

        }
        data.setBinary(binaryManufacturer);
        return data;
    }

    public String ShouldReturnAByteStringManufacturer() {
        String retour = "";
        char[] manu = this.barcode.getManufacturer().toCharArray();
        for (int i = 0; i < leftEncoding().length; i++) {
            if (leftEncoding()[i] == 'L') {
                retour = retour + Ds.getInMemoryDAO().fetchLCode(Byte.valueOf(String.valueOf(manu[i])));
            } else if (leftEncoding()[i] == 'G') {
                retour = retour + Ds.getInMemoryDAO().fetchGCode(Byte.valueOf(String.valueOf(manu[i])));
            }
        }
        return retour;
    }

    @Override
    public IBinaryData encodeProduct() {
        ByteBinaryData data = new ByteBinaryData();
        String[] retour = new String[0];
        byte[] binaryEncodingProd = new byte[shouldreturnAByteStringProduct().length()];
        for (int i = 0; i < shouldreturnAByteStringProduct().length(); i++) {
            binaryEncodingProd[i] = Byte.valueOf(String.valueOf(shouldreturnAByteStringProduct().charAt(i)));
        }
        data.setBinary(binaryEncodingProd);
        return data;
    }

    public String shouldreturnAByteStringProduct() {

        String retour = "";
        char[] product =(this.barcode.getProduct()+String.valueOf(getCheksum())).toCharArray();
        for (int i = 0; i < rightEncoding().length; i++) {
            if (rightEncoding()[i] == 'R') {
                retour = retour + Ds.getInMemoryDAO().fetchRCode(Byte.valueOf(String.valueOf(product[i])));
            }
        }
        return retour;
    }

    /*this function needs to be perfectionnize. It is the same as getLeftGuard*/
    @Override
    public IBinaryData getRightGuard() {
        ByteBinaryData data = new ByteBinaryData();

        String[] rightGuard = {"1", "0", "1"};
        byte[] binary = new byte[rightGuard.length];
        for (int i = 0; i < rightGuard.length; i++) {
            binary[i] = Byte.valueOf(rightGuard[i]);
        }
        data.setBinary(binary);
        return data;
    }

    @Override
    public IBinaryData getLeftGuard() {
        ByteBinaryData data = new ByteBinaryData();
        String[] leftGuard = {"1", "0", "1"};
        byte[] binary = new byte[leftGuard.length];
        for (int i = 0; i < leftGuard.length; i++) {
            binary[i] = Byte.valueOf(leftGuard[i]);
        }
        data.setBinary(binary);
        return data;
    }

    public int getSumOfDecimal() {
        int index = 2;
        int checksum = 0;
        char[] dec = (this.barcode.getCountry() + this.barcode.getManufacturer() + this.barcode.getProduct()).toCharArray();
        for (int i = 0; i < dec.length; i++) {
            if (i % 2 == 0) {
                checksum = checksum + (Integer.parseInt(String.valueOf(dec[i])) * 1);
            } else {
                checksum = checksum + (Integer.parseInt(String.valueOf(dec[i])) * 3);
            }
        }
        return checksum;
    }

    public int getCheksum() {
        char[] dec =String.valueOf(getSumOfDecimal()).toCharArray();
        int retour =0;
        int unit = 10;
        for (int i = 0; i < dec.length; i++) {
                retour =  unit - (Integer.parseInt(String.valueOf(dec[i])));
        }
        if (retour ==10)
        {
            retour=0;
        }
        return retour;
    }

    @Override
    public IBinaryData getSeparator() {
        ByteBinaryData data = new ByteBinaryData();
        String[] leftGuard = {"0", "1", "0", "1", "1"};
        byte[] binarySeparator = new byte[leftGuard.length];
        for (int i = 0; i < leftGuard.length; i++) {
            binarySeparator[i] = Byte.valueOf(leftGuard[i]);
        }
        data.setBinary(binarySeparator);
        return data;

    }

    //This method will return an array of char from leftCode
    public char[] leftEncoding() {

        String leftPattern = Ds.getInMemoryDAO().fetchEAN13EncodingPattern(Byte.valueOf(this.barcode.getCountry())).getLeftGroupPattern();
        char[] leftCode = leftPattern.toCharArray();
        return leftCode;
    }

    public char[] rightEncoding() {
        String rightPattern = Ds.getInMemoryDAO().fetchEAN13EncodingPattern(Byte.valueOf(this.barcode
                .getCountry())).getRightGroupPattern();
        char[] RightCode = rightPattern.toCharArray();
        return RightCode;
    }

    /*Generate*/
    public IBinaryData generateBinaryCode() {
        ByteBinaryData data = new ByteBinaryData();
        ByteBinaryData retour = new ByteBinaryData();
        if (data.append(getLeftGuard()) == true) {
            if (data.append(encodeManufacturer()) == true) {
                if (data.append(getSeparator()) == true) {
                    if (data.append(encodeProduct()) == true) {
                        if (data.append(getRightGuard()) == true) {
                            retour = data;
                        }
                    }
                }
            }
        }
        return retour;
    }

}
