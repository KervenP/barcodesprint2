/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.MDL;

/**
 *
 * @author PolidorArte
 */
public abstract class AbstractEAN13_Engine extends AbstractBarcodeEngine{
    
    public AbstractEAN13_Engine(Barcode barcode) {
        super(barcode);
    }

    @Override
    public Barcode getBarcode() {
        return barcode;
    }

    @Override
    public void setBarcode(Barcode barcode) {
        this.barcode = barcode;
    }
    
}
