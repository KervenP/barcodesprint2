/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BLL.MDL;

/**
 *
 * @author PolidorArte
 */
public interface IBarcodeEngine {
      
    public IBinaryData encodeManufacturer();

    public IBinaryData encodeProduct();

 
    public IBinaryData getRightGuard(); 


    public IBinaryData getLeftGuard();

    
    public IBinaryData getSeparator();

        

}
