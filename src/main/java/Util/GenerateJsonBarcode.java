/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;



import com.google.gson.Gson;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author PolidorArte
 */
public class GenerateJsonBarcode {
    String countryName;
public String dec;
public String binary;

public GenerateJsonBarcode(String countryName,String dec, String binary){
    this.dec=dec;
    this.binary=binary;
    this.countryName=countryName;
}
public GenerateJsonBarcode(){
}
 /*  public static JSONObject objToJson(String decimal,String binarycode){
   JSONObject obj=new JSONObject();
   if(decimal.length()==14)
   {
       if (binarycode.length()==95)
         obj.put(decimal, binarycode);
    }
   return obj;
   }   */

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getDec() {
        return dec;
    }

    public void setDec(String dec) {
        this.dec = dec;
    }

    public String getBinary() {
        return binary;
    }

    public void setBinary(String binary) {
        this.binary = binary;
    }
}
