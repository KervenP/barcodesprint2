/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 *
 * @author PolidorArte
 */
public class BarcodeJson {
    private String deciamlBarcode;
    private String binaryBarocode;

    public BarcodeJson(String deciamlBarcode, String binaryBarocode) {
        this.deciamlBarcode = deciamlBarcode;
        this.binaryBarocode = binaryBarocode;
    }

    public BarcodeJson() {
    }

    public String getDeciamlBarcode() {
        return deciamlBarcode;
    }

    public void setDeciamlBarcode(String deciamlBarcode) {
        this.deciamlBarcode = deciamlBarcode;
    }

    public String getBinaryBarocode() {
        return binaryBarocode;
    }

    public void setBinaryBarocode(String binaryBarocode) {
        this.binaryBarocode = binaryBarocode;
    }

    @Override
    public String toString() {
        return "BarcodeJson{" + "deciamlBarcode=" + deciamlBarcode + ", binaryBarocode=" + binaryBarocode + '}';
    }
    
}
