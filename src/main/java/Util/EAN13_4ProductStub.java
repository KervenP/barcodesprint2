/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import BLL.MDL.ByteBinaryData;
import BLL.MDL.EAN13_4Product;
import BLL.MDL.IBinaryData;
import BLL.MDL.IEAN13_4Product;
import java.util.Random;

/**
 *
 * @author PolidorArte
 */
public class EAN13_4ProductStub {
public static String leftguard = "101";
   public static  String rightGuard = "101";
    public   static String center = "01010";
    public static String generateBinaryCode(String countryID, String manufacturerId, String productId) {

        String manufacture = "";
        String product="";
       ;
        for (int i = 0; i < manufacturerId.length(); i++) {
            manufacture = manufacture + getRandom();
            product=product+getRandom();
        }
       
        return leftguard+manufacture+center+product+rightGuard;
    }

    public static String getRandom() {
        Random random = new Random();
        String retour = "";
        for (int j = 0; j < 7; j++) {
            retour = retour + random.nextInt(2);
        }
        return retour;
    }
    
}
