/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import BLL.Service.BarcodeDecodingService;
import DAL.DataService;
import Util.EAN13_4ProductStub;
import Util.GenerateJsonBarcode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

/**
 *
 * @author PolidorArte
 */
public class MainMock {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(EAN13_4ProductStub.generateBinaryCode("1", "1234578","12345"));
          String dec="1"+ "1234578"+"12345";
               GenerateJsonBarcode code=new GenerateJsonBarcode("canada",dec,EAN13_4ProductStub.generateBinaryCode("1", "1234578","12345"));
               Gson gson=new GsonBuilder().create();
               JsonElement element;
               element=gson.toJsonTree(code,GenerateJsonBarcode.class);
               BarcodeDecodingService.insertDocToHistoryDao(element);
        DataService.getInstance().getEncodingHistory().fetchAllFromMongoDb().forEach((s) ->  {
            System.out.println(s);
        });
    }
    
        
}
