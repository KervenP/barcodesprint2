/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import BLL.Service.BarcodeDecodingService;
import javafx.geometry.Pos;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;


/**
 *
 * @author PolidorArte
 */
public class CanvaRendering {

    //Faire une méthode qui déssine un canva selon un nombre en entrée
    public static String barcodeCanva(String countryId,String manufactId,String prodId) {
      String x=BarcodeDecodingService.getEAN13BinaryCodeForProduct(countryId, manufactId, prodId);
      if(x!=null){
       return x;
      } else return null;
    
    }

    public static void drawCanva(GraphicsContext gc,String countryId,String manufactId,String prodId){
    
        int space = 0;
         
        for (int i = 0; i < barcodeCanva(countryId, manufactId, prodId).length(); i++) {
            if (barcodeCanva(countryId, manufactId, prodId).charAt(i) == ('1')) {
                gc.setFill(Color.BLACK);
                gc.fillRect(50 + space, 50,2, 100);
                                
            } else {
                gc.setFill(Color.WHITE);
                gc.fillOval(50 + space, 50, 2, 100);
            }
            space += 2;
        }    

gc.setLineWidth(1.1);

gc.strokeText(countryId+"-"+manufactId+"-"+prodId,75,160);
 
    }

}
