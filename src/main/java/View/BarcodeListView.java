/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import BLL.Service.BarcodeDecodingService;
import Util.GenerateJsonBarcode;
import java.awt.Insets;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author PolidorArte
 */
public class BarcodeListView {
 public static ListView History;
 public  static Scene scene;
 public static Button BackBtn,ExitBtn;

    /**
     *
     * @return
     */
    public static Scene listHistory(Stage stage,Scene s){
       VBox groop=new VBox();
       
       GridPane grid=new GridPane();
       BackBtn=new Button("Back");
       BackBtn.setPrefSize(50,20);
       ExitBtn=new Button("Exit");
         ExitBtn.setPrefSize(50,20);
       grid.add(BackBtn,0,1);
       grid.setHgap(20);
       grid.setPrefSize(50,20);
       grid.setVgap(20);
       grid.add(ExitBtn,1,1);
       grid.setAlignment(Pos.CENTER);
       
       scene=new Scene(groop,1000,500);
       
       TableView<GenerateJsonBarcode> table=new TableView();
       table.setPrefWidth(980);
     
       ObservableList<GenerateJsonBarcode> history=FXCollections.observableArrayList(BarcodeDecodingService.listHistory());
       table.setItems(history);
       TableColumn<GenerateJsonBarcode,String>countryNameColl=new TableColumn<>("Country");
       countryNameColl.setCellValueFactory(new PropertyValueFactory("countryName"));
       TableColumn<GenerateJsonBarcode,String>decNum=new TableColumn<>("Code");
       decNum.setCellValueFactory(new PropertyValueFactory("dec"));
       TableColumn<GenerateJsonBarcode,String>binaryColl=new TableColumn<>("Binary");
       binaryColl.setCellValueFactory(new PropertyValueFactory("binary"));
        table.getColumns().setAll(countryNameColl,decNum,binaryColl);
      // History.setItems(history);
       groop.getChildren().addAll(table,grid);
       ExitBtn.setOnAction((ActionEvent event) -> {
           stage.close();
       });
       BackBtn.setOnAction((ActionEvent event) -> {
           stage.setScene(s);
       });
       groop.setStyle("-fx-Background-Color: Beige;");
       return scene;
    }
    
}
