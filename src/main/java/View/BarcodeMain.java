/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import BLL.Service.BarcodeDecodingService;
import Util.GenerateJsonBarcode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.mongodb.BasicDBObject;
import java.io.File;
import javafx.application.Application;
import static javafx.application.Application.launch;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.geometry.Insets;
import javafx.geometry.Pos;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.*;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.swing.Action;
import org.json.simple.JSONObject;

/**
 *
 * @author PolidorArte
 */
public class BarcodeMain extends Application {

    public static Label countryLabel, countryCodeLabel,
            manufacturerCodeLabel, ProductCodeLabel;
    //Combobox declaration 
    public static ComboBox countryComboBox, countryCodeComboBox;
//ObservableList
    ObservableList<String> countrynames, countryCodes;
//TextFiel delaclaration
    public static TextField manufacturerField, productCodeField;
    //Butons Declaration 
    public static Button getBarcodeBtn, countrycodesBtn, downloadToPdfBtn, exitbtn;
    //ALl Strings declaration
    public String countryName, manufacturerId, prodId;
    //graphics context declaration
    GraphicsContext gc;
    public String country;
    public static Scene s;
    public static Stage primary;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Barcode v1.0 -Peter Pan");

        //*******Gripane to receive all the option needed*******/
        VBox root = new VBox();
        s = new Scene(root, 500, 500, Color.BLACK);
        
        root.setAlignment(Pos.CENTER);
        primaryStage.setResizable(false);
        GridPane mainContainer = new GridPane();

        mainContainer.setVgap(20);
        mainContainer.setHgap(20);
        mainContainer.setAlignment(Pos.CENTER);
        //**************setting Gripane size******************/
        mainContainer.setMinSize(300, 100);
        //setting padding
        mainContainer.setPadding(new Insets(20, 20, 20, 20));
        //****************************Label edition*****************/
        countryLabel = new Label("Country");
        countryCodeLabel = new Label("Country code");
        manufacturerCodeLabel = new Label("Manufacturer code");
        ProductCodeLabel = new Label("Product code");

        //*************Ajout des label dans le mainContainer*************/*
        mainContainer.add(countryLabel, 0, 1);
        mainContainer.add(countryCodeLabel, 0, 2);
        mainContainer.add(manufacturerCodeLabel, 0, 3);
        mainContainer.add(ProductCodeLabel, 0, 4);
        /**
         * *************Iniatilisation des boutton et ajout dans MainCOntainer*
         */
        getBarcodeBtn = new Button("Get Barcode");
        getBarcodeBtn.setPrefWidth(100);
        countrycodesBtn = new Button("Country codes");
        countrycodesBtn.setPrefWidth(100);
        downloadToPdfBtn = new Button("Download to Pdf");
        downloadToPdfBtn.setPrefWidth(100);
        exitbtn = new Button("Exit");
        exitbtn.setPrefWidth(100);
        /**
         * *************adding the button to the mainContainer**************
         */
        mainContainer.add(getBarcodeBtn, 2, 1);
        mainContainer.add(countrycodesBtn, 2, 2);
        mainContainer.add(downloadToPdfBtn, 2, 3);
        mainContainer.add(exitbtn, 2, 4);
        /**
         * *******Combobox setUp*********************************
         */
        //Observeable List to feed the combobox
        countrynames = FXCollections.observableArrayList(BarcodeDecodingService.getAllCountryFromDB());

        countryComboBox = new ComboBox(countrynames);
        countryComboBox.setPrefWidth(150);
        countryComboBox.setVisibleRowCount(3);

        //**On Country Selected display all related number 
        countryCodeComboBox = new ComboBox();
        countryCodeComboBox.setPrefWidth(150);
        countryCodeComboBox.setVisibleRowCount(3);

        //**adding the combobox to the mainContainer
        mainContainer.add(countryComboBox, 1, 1);
        mainContainer.add(countryCodeComboBox, 1, 2);

        /**
         * *******Setting Up TextField **************************
         */
        //***manufacturerField
        manufacturerField = new TextField();
        Tooltip manufacture = new Tooltip();
        manufacture.setText("\nYour manufacture must be \n"
                + "at least 5 number");
        manufacturerField.setTooltip(manufacture);
        //***productCodeField 
        productCodeField = new TextField();
        Tooltip productip = new Tooltip("\nYour Product must be"
                + "at least 6 number.");
        productCodeField.setTooltip(productip);

        mainContainer.add(manufacturerField, 1, 3);
        mainContainer.add(productCodeField, 1, 4);
        ///**************Draw a line And Canva Style******************////

        Canvas canva = new Canvas(300,300);
        canva.setStyle("-fx-Background-Color: white; ");
        gc = canva.getGraphicsContext2D();

        //***************all the action for the button********************///
        countryComboBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                countryName = countryComboBox.getValue().toString();

                countryCodes = FXCollections.observableArrayList(BarcodeDecodingService.fecthCodesByCountryName("'" + countryName + "'"));

                countryCodeComboBox.setItems(countryCodes);
            }
        });

        getBarcodeBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gc.clearRect(0, 0, canva.getWidth(), canva.getHeight());
                manufacturerId = String.valueOf(manufacturerField.getText());
                prodId = String.valueOf(productCodeField.getText());
                country = String.valueOf(countryCodeComboBox.getValue());
                CanvaRendering.drawCanva(gc, country, manufacturerId, prodId);
                //save json file to historyDAO
                String dec = country + manufacturerId + prodId;
                GenerateJsonBarcode code = new GenerateJsonBarcode(countryName, dec, BarcodeDecodingService.getEAN13BinaryCodeForProduct(country, manufacturerId, prodId));
                Gson gson = new GsonBuilder().create();
                JsonElement element;
                element = gson.toJsonTree(code, GenerateJsonBarcode.class);
                BarcodeDecodingService.insertDocToHistoryDao(element);
            }
        });
        countrycodesBtn.setOnAction((ActionEvent e) -> {
            primaryStage.setScene(BarcodeListView.listHistory(primaryStage, s));
        });

        exitbtn.setOnAction((ActionEvent event) -> {
            primaryStage.close();
        });
        /**
         * ******************************************************************
         */
        //****Calling the css file ****/
    
       root.setStyle("-fx-Background-Color: Beige;-fx-Content-Alignment:center;-fx-text-alignement:center;");
       
    canva.setStyle("-fx-Height: 200px; -fx-Backgroud-color: white;");
        root.getChildren().addAll(mainContainer, canva);
        primaryStage.setScene(s);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

 

}
